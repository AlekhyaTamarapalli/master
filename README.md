## Addition Tutor

**How to run this project?**

1. Install Visual Studio
2. Open AdditionTutor -> AdditionTutor.sln in visual studio
3. Click on the **Start** button from the tool bar on the top to start the project.

**How to use this project?**

1. Choose the operator from the available options in the Window
2. Calculate and feed the value in the textbox, given adjacent to the operands.
3. Click on "Check Answer" button to verify your input.
4. Click on "Try Again" re-enter the answer in case if you are wrong.
4. Click on "Reset" to start the tutorial again.

**License**
The GNU General Public License Version 3, is used since it is a free software license that guarantee end users the freedom to run, study, share the software.
